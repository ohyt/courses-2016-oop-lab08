package it.unibo.oop.lab.reflection02;

/**
 * Act as a convenience JUnit test of a robot factory.
 * 
 */
public class TestComposableRobot {

    /**
     * Test a Robot Factory.
     */

    // TODO uncomment the following test after completing the assignment so as to test your solution.
//    @Test
//    public void testParts() {
//        final RobotArm left = new RobotArm("Left arm");
//        final RobotArm right = new RobotArm("Right arm");
//        final RobotHead head = new RobotHead("head");
//
//        // turning parts on
//        try {
//            left.turnOn();
//            right.turnOn();
//            head.turnOn();
//        } catch (CantTurnOnException e) {
//            fail("Test failed: parts must be activated with no issues");
//        }
//        try {
//            left.turnOn();
//            fail("Left arm cannot be activated againg");
//        } catch (CantTurnOnException e) {
//            System.out.println("Left arm already active");
//        }
//        try {
//            right.turnOff();
//            head.turnOff();
//        } catch (CantTurnOnException e) {
//            fail("Test failed: parts must be deactivated with no issues");
//        }
//        try {
//            right.turnOff();
//            fail("Test failed: right arm already off");
//        } catch (CantTurnOnException e) {
//            System.out.println("Right arm is already off");
//        }
//    }
}